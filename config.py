#!/usr/bin/env python

import ConfigParser

class Config:
    def __init__(self):
      self.DEFAULTCONFIG = 'ripza.cfg'
    def defaultConf(self):
      config = ConfigParser.SafeConfigParser()
      #sections
      config.add_section('Rip')
      config.add_section('Youtube Account')
      #main ripper section vars
      config.set('Rip','quality','best')
      config.set('Rip','ignore errors','true')
      config.set('Rip','use title as filename','true')
      #youtube account section vars
      config.set('Youtube Account','username','')
      config.set('Youtube Account','password','')

      with open(self.DEFAULTCONFIG,'wb') as configfile:
        config.write(configfile)

