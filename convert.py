#!/usr/bin/env python
"""
Convertion part for TSR converts the supplyed flv's to vairus formats
using ffmpeg.

"""

# Copyright 2010 Bug
# This file is part of TSR (The stream ripper).
#
# TSR is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TSR is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TSR.  If not, see <http://www.gnu.org/licenses/>.

import os, sys, ConfigParser, subprocess, getopt
HOME = os.getenv('USERPROFILE') or os.getenv('HOME')
if os.path.isfile(HOME + '/.tsr.cfg'):
    CONFIGFILE = 'tsr.cfg'
elif os.path.isfile('tsr.cfg'):
    CONFIGFILE = 'tsr.cfg'
else:
    CONFIGFILE = None

VERSION = '0.0.001'

def readconfigvideo():
    """Reads the config and returns the video format to be used"""
    config = ConfigParser.SafeConfigParser({'video': 'avi'})
    if CONFIGFILE:
        config.read(CONFIGFILE)
        return config.get('Convert', 'video')
    else:
        return 'avi'

def readconfigaudio():
    """Reads the config and returns the audio format to be used"""
    config = ConfigParser.SafeConfigParser({'video': 'oga'})
    if CONFIGFILE:
        config.read(CONFIGFILE)
        return config.get('Convert', 'audio')
    else:
        return 'oga'

def readconfigffmpeg():
    """Reads the config and returns ffmpeg path"""
    config = ConfigParser.SafeConfigParser({'video': 'ffmpeg'})
    if CONFIGFILE:
        config.read(CONFIGFILE)
        return config.get('Convert', 'ffmpeg')
    else:
        return 'ffmpeg'

def getfilename(filepath):
    """Returns the name of the file without the extention"""
    if os.path.isfile(filepath):
        name = os.path.split(filepath)
        name = os.path.splitext(name[1])
        return name[0] 
    else:
        return None

def convert(name, targetformat=None):
    """Converts given file to a given format and returns new file name"""
    if targetformat is None:
        targetformat = readconfigvideo()
    #Still gotta fix the ffmpeg call...
    #Also gotta make it non depanded no ffmpeg and capable of using 
        #other convertors...
    #And figure a better way to control the command.
    command = [readconfigffmpeg(), '-i', name]
    # Specail switches per format
    if targetformat == 'oga':
        command.append('-vn')
        command.append('-f')
        command.append('ogg')
    newname = getfilename(name) + '.' + targetformat
    command.append(newname)
    subprocess.Popen(command, stderr=subprocess.STDOUT, \
    stdout=subprocess.PIPE).communicate()[0]
    return newname

def help_message():
    """Print an help message for the program and quits"""
    print '''
 {0} converts a file to another format using ffmpeg

options: 
    -h or --help    Display this help message
    -e or --encoding extention
        Forces the convertor to convert to that format
    -a or --audio-only  Convert to audio only
    -v or --video       Convert both audio and video
        --encoding, --audio and --video should not be used at the same
            time
    --version       Display version information

This is part of The Stream Ripper.'''.format(sys.argv[0])
    sys.exit(0)
def version_message():
    """Prints version information"""
    print """
        Version {0}""".format(VERSION)
    sys.exit(0)

def main():
    """Self debug and other stuff?"""


    try:
        options, xarguments = getopt.getopt(sys.argv[1:], 'he:av', \
        ['help', 'encoding:', 'audio-only', 'video', 'version'])
    except getopt.error:
        print 'Error: You tried to use an unknown option or the'
        print 'argument for an option that requires it was missing.'
        print 'Try ' + sys.argv[0] + ' -h for more information.'
        sys.exit(0)

    # Assumptions so code won't die
    target = None

    for a in options[:]:
        if a[0] == '-h' or a[0] == '--help':
            help_message()
    for a in options[:]:
        if a[0] == '--version':
            version_message()
    for a in options[:]:
        if a[0] == '-e' or a[0] == '--encoding':
            target = a[1]
            options.remove(a)
            break
    for a in options[:]:
        if a[0] == '-a' or a[0] == 'audio':
            target = readconfigaudio()
            options.remove(a)
            break
    for a in options[:]:
        if a[0] == '-v' or a[0] == 'video':
            target = readconfigvideo()
            options.remove(a)
            break
    if not xarguments:
        print 'Syntax: {0} [-e encoding / -a / -v] file'.format( \
        sys.argv[0])
        sys.exit()
    for name in xarguments:
        convert(name, target)

    return 0

if __name__ == '__main__':
    main()
