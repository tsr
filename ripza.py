#!/usr/bin/env python

import sys,getopt,ConfigParser,subprocess


class Ripza:
	def __init__(self):
		self.DEFAULTCONFIG = 'ripza.cfg'
		self.quality = ''
		self.ignoreErrors = 'true'
		self.username = ''
		self.password = ''
		self.title = 'true'
	def readConf(self):
		config = ConfigParser.SafeConfigParser()
		config.read(self.DEFAULTCONFIG)
		self.quality = config.get('Rip','quality')
		self.ignoreErrors = config.getboolean('Rip','ignore errors')
		self.title = config.getboolean('Rip','use title as filename')
		self.username = config.get('Youtube Account','username')
		self.password = config.get('Youtube Account','password')
	
	def callYouTubeDl(self,uri):
		command = ['youtube-dl']
		if self.ignoreErrors:
			command.append('-i')
		if self.quality != '':
			#insert quality switch here
			print 'too much work and no play make' 
			print 'little ripza an uncompilable proggie'
		if self.title:
			command.append('-t')
		subprocess.Popen(command,stderr = subprocess.STDOUT, \
						 stdout = subprocess.PIPE).communicate()[0]
		return 'done'
